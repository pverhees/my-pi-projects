import sys
import string
def main():
    numBlanks = 4
	
    for i in range(1, 10, 2):
		# print numBlanks blanks, and i asterisks on line i
		for j in range(0, numBlanks):
			# print a blank space
			sys.stdout.write(" ")

		for j in range(i):
			sys.stdout.write("*")
		
		numBlanks -= 1 # next line has fewer blanks

		# go to new line
		print

	# print base/trunk of tree
print string.center("**", 9)
print string.center("**", 9)



main()